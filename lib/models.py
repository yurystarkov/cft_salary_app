from pydantic import BaseModel
from datetime import date


class Salary(BaseModel):
    amount: float
    promotion: date | None = None


class Employee(BaseModel):
    username: str
    fullname: str
    password: str
    salary: Salary


class Employees(BaseModel):
    __root__: list[Employee]

    def __iter__(self):
        return iter(self.__root__)
