from fastapi import FastAPI
import json

from lib.models import Employees

app = FastAPI()

with open("employees_db.json") as db:
    employees = Employees.parse_obj(json.load(db))


@app.post("/login")
async def login(username: str, password: str):
    for employee in employees:
        if employee.username == username and employee.password == password:
            return employee.token


@app.get("/salary/{username}")
async def salary(username: str):
    for employee in employees:
        if employee.username == username:
            return employee.salary


if __name__ == "__main__":
    import uvicorn
    uvicorn.run("main:app", host="127.0.0.1", port=8000, reload=True)
